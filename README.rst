========================================
Attain Corp. vocabulary file to Anki CSV
========================================

Automatically construct CSV with Kanji, Hiragana, English, and Kanji meanings that can be fed to Anki.
Kanji meanings are scraped from Jisho.

Usage
=====

Run ``poetry install`` to install dependencies. Run ``poetry run main file.csv`` to run the script on the file ``file.csv``.

Author
======

Jan Tuomi, <jans.tuomi@gmail.com>
