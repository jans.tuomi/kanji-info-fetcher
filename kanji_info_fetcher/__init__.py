import argparse
import kanji_info_fetcher.io
import kanji_info_fetcher.jisho_client
import kanji_info_fetcher.wadoku_client

__version__ = '0.1.0'

DEFAULT_FORMAT = 'kanji,english'

def main():
  parser = argparse.ArgumentParser(description='Convert course vocab into Anki deck')
  parser.add_argument('--format', metavar='format', type=str, default=DEFAULT_FORMAT,
                      help='comma-separated row format, e.g. kanji,hiragana,english,readings')
  parser.add_argument('file', metavar='file', type=str,
                      help='vocabulary file path')

  args = parser.parse_args()
  file_path = args.file
  row_format = args.format

  print("row_format:", row_format)

  # read entries from file
  print(f"[STATUS] Reading file {file_path}")
  entries = io.read_from_file(file_path, row_format)

  # fetch translations from Jisho
  print(f"[STATUS] Fetching English translations and example sentences from Jisho")
  entries = jisho_client.inject_english_translation_and_note(entries)

  # fetch kanji meanings from Jisho
  print(f"[STATUS] Fetching kanji meaning data from Jisho")
  entries = jisho_client.inject_kanji_meanings(entries)

  # fetch hiragana readings from Wadoku w/ pitch accents
  print(f"[STATUS] Fetching reading data from Wadoku")
  entries = wadoku_client.inject_hiragana_with_accents(entries)

  # write entries to file
  out_file_name = file_path.replace(".csv", ".out.csv") if ".csv" in file_path else f"{file_path}.out"
  print(f"[STATUS] Writing result to file {out_file_name}")
  io.write_to_file(out_file_name, entries)
