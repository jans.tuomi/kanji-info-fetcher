from bs4 import BeautifulSoup
import requests
import urllib.parse

WADOKU_BASE_URL = "https://www.wadoku.de/search"

def inject_hiragana_with_accents(entries: list) -> list:
  entries_with_readings = []

  for index, entry in enumerate(entries):
    kanji = entry.kanji.replace("~", "")

    if len(entry.hiragana) > 0:
      print(f"Hiragana exists for #{index}: {kanji}. Skipping...")
      entries_with_readings.append(entry)
      continue

    # Remove nasty characters from kanji since Wadoku
    # fails to find anything otherwise
    print(f"Fetching hiragana data for #{index}: {kanji}")

    resource_part = urllib.parse.quote_plus(kanji)
    url = f"{WADOKU_BASE_URL}/{resource_part}"
    resp = requests.get(url)
    resp.raise_for_status()

    soup = BeautifulSoup(resp.text, "html.parser")
    container = soup.select_one("section#content")

    readings = container.select("span.reading")
    first_reading = str(readings[0])

    # Remove extra characters added by Wadoku
    # https://www.wadoku.de/wiki/display/WAD/Hinweise+zur+Notation
    result = first_reading \
      .replace("~", "") \
      .replace("|", "") \
      .replace("￨", "") \
      .replace("・", "") \
      .replace("･", "")
    entry.hiragana = result
    entries_with_readings.append(entry)

  return entries_with_readings
