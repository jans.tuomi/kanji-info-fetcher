from dataclasses import dataclass

@dataclass
class Entry:
  kanji: str
  hiragana: str
  english: str
  meanings: str
  note: str
